import { HardhatUserConfig } from "hardhat/config";
import '@typechain/hardhat';
import 'hardhat-deploy';
import "@nomicfoundation/hardhat-toolbox";
import '@nomicfoundation/hardhat-verify';
import * as dotenv from 'dotenv';
import './tasks';

const env = dotenv.config();

const DEPLOYMENT_PRIVATE_KEY = env.parsed?.DEPLOYMENT_PRIVATE_KEY;

const accounts = DEPLOYMENT_PRIVATE_KEY ? [DEPLOYMENT_PRIVATE_KEY] : [];
const polygonScanApiKey = env.parsed?.POLYGONSCAN_API_KEY ?? '';
const snowtraceApiKey = env.parsed?.SNOWTRACE_API_KEY ?? '';
const etherScanApiKey = env.parsed?.ETHERSCAN_API_KEY ?? '';
const bscScanApiKey = env.parsed?.BSCSCSAN_API_KEY ?? '';

const config: HardhatUserConfig = {
  solidity: {
    compilers: [
      {
        version: "0.8.7",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        }
      },
      {
        version: "0.8.19",
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        }
      },
    ]
  },
  paths: {
    sources: "./contracts",
    tests: "./test",
    cache: "./cache",
    artifacts: "./artifacts"
  },
  mocha: {
    timeout: 40000
  },
  networks: {
    mainnet: {
      url: 'https://1rpc.io/eth',
      accounts
    },
    bsc: {
      url: 'https://bsc-dataseed1.binance.org/',
      accounts
    },
    avalanche: {
      url: "https://api.avax.network/ext/bc/C/rpc",
      accounts
    },
    polygon: {
      url: "https://polygon-rpc.com/",
      accounts,
      gasPrice: 300_000000000,
      gasMultiplier: 2,
    },
  },
  etherscan: {
    apiKey: {
      avalanche: snowtraceApiKey,
      polygon: polygonScanApiKey,
      bsc: bscScanApiKey,
      mainnet: etherScanApiKey
    }
  },
  typechain: {
    outDir: 'typechain',
    target: 'ethers-v6',
  },
};

export default config;
