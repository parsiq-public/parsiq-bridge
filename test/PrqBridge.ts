import { loadFixture } from '@nomicfoundation/hardhat-toolbox/network-helpers';
import { expect } from 'chai';
import { ethers } from 'hardhat';
import { getBigInt, parseUnits } from "ethers";

const BSC_CHAIN_ID = 56;
const ETH_CHAIN_ID = 1;

describe('PrqBridgeTest', function () {
  async function deployPrqBridgeFixture() {
    const [owner, otherAccount] = await ethers.getSigners();

    const PrqEthMock = await ethers.getContractFactory('PrqEthMock');
    const PrqBscMock = await ethers.getContractFactory('PrqBscMock');
    const prqBsc = await (await PrqBscMock.deploy()).waitForDeployment();
    const prqEth = await (await PrqEthMock.deploy()).waitForDeployment();

    const DeBridgeGate = await ethers.getContractFactory('DeBridgeGateMock');
    const deBridgeGate = await (await DeBridgeGate.deploy()).waitForDeployment();

    const TokenControllerBsc = await ethers.getContractFactory('TokenControllerBsc');
    const tokenControllerBsc = await (await TokenControllerBsc.deploy(prqBsc.target)).waitForDeployment();

    const TokenControllerEth = await ethers.getContractFactory('TokenControllerEth');
    const tokenControllerEth = await (await TokenControllerEth.deploy(prqEth.target)).waitForDeployment();

    const PrqBridgeTest = await ethers.getContractFactory('PrqBridgeTest');
    const prqBridgeChainBsc = await (
      await PrqBridgeTest.deploy(deBridgeGate.target, tokenControllerBsc, BSC_CHAIN_ID)
    ).waitForDeployment();
    const prqBridgeChainEth = await (
      await PrqBridgeTest.deploy(deBridgeGate.target, tokenControllerEth, ETH_CHAIN_ID)
    ).waitForDeployment();

    await prqEth.unpause();

    await prqBsc.authorizeBridge(tokenControllerBsc.target);

    await prqBridgeChainBsc.setDeBridgeGate(deBridgeGate.target);
    await prqBridgeChainEth.setDeBridgeGate(deBridgeGate.target);
    await prqBridgeChainBsc.setCounterChainContract(ETH_CHAIN_ID, prqBridgeChainEth.target);
    await prqBridgeChainEth.setCounterChainContract(BSC_CHAIN_ID, prqBridgeChainBsc.target);
    await tokenControllerBsc.setBridgeContract(prqBridgeChainBsc.target);
    await tokenControllerEth.setBridgeContract(prqBridgeChainEth.target);

    await deBridgeGate.setChainIdAndContractForMsgSender(
      prqBridgeChainBsc.target,
      ETH_CHAIN_ID,
      prqBridgeChainEth.target,
    );
    await deBridgeGate.setChainIdAndContractForMsgSender(
      prqBridgeChainEth.target,
      BSC_CHAIN_ID,
      prqBridgeChainBsc.target,
    );

    // provide random supply on BSC to test mint/burn logic
    await prqBsc.authorizeBridge(owner);
    await prqBsc.connect(owner).mint(owner, parseUnits((Math.random() * 1000 + 10).toString(), 18));
    await prqBsc.deauthorizeBridge(owner);

    return {
      prqBsc,
      prqEth,
      deBridgeGate,
      tokenControllerBsc,
      tokenControllerEth,
      prqBridgeChainBsc,
      prqBridgeChainEth,
      owner,
      otherAccount,
    };
  }

  describe('Deployment', function () {
    it('should transfer tokens from bsc to eth and alter (burn) supply accordingly', async function () {
      const {
        prqBsc,
        prqEth,
        deBridgeGate,
        tokenControllerBsc,
        tokenControllerEth,
        prqBridgeChainBsc,
        prqBridgeChainEth,
        owner,
        otherAccount,
      } = await loadFixture(deployPrqBridgeFixture);

      // move some liquidity to bridge on ETH
      await prqEth.connect(owner).transfer(tokenControllerEth.target, parseUnits('10000', 18));
      const randomSupplyPercent = (Math.random() * 50 + 1).toFixed();

      // Specify amount for transfer and pretend-chain IDs
      const recipient = otherAccount.address; // Transfer to 'otherAccount'
      const bscInitialTotalSupply = await prqBsc.totalSupply();
      const amountToTransfer = (bscInitialTotalSupply / 100n) * getBigInt(randomSupplyPercent);

      const ownerInitialBalance = await prqBsc.balanceOf(owner);
      const recipientInitialBalance = await prqEth.balanceOf(recipient);

      await prqBsc.connect(owner).approve(tokenControllerBsc, amountToTransfer);

      await prqBridgeChainBsc.connect(owner).sendToChain(ETH_CHAIN_ID, amountToTransfer, recipient, {
        value: await deBridgeGate.globalFixedNativeFee(),
      });

      await deBridgeGate.callUnlock(prqBridgeChainEth.target, BSC_CHAIN_ID, amountToTransfer, recipient);

      expect(await prqEth.balanceOf(recipient)).to.equal(recipientInitialBalance + amountToTransfer);
      expect(await prqBsc.balanceOf(owner)).to.equal(ownerInitialBalance - amountToTransfer);
      expect(await prqBsc.totalSupply()).to.equal(bscInitialTotalSupply - amountToTransfer);
    });

    it('should transfer tokens from eth to bsc and alter (mint) supply accordingly', async function () {
      const {
        prqBsc,
        prqEth,
        deBridgeGate,
        tokenControllerEth,
        prqBridgeChainBsc,
        prqBridgeChainEth,
        owner,
        otherAccount,
      } = await loadFixture(deployPrqBridgeFixture);

      // Specify amount for transfer and pretend-chain IDs
      const amountToTransfer = parseUnits('10', 18);
      const recipient = otherAccount.address; // Transfer to 'otherAccount'
      const bscInitialTotalSupply = await prqBsc.totalSupply();

      const ownerInitialBalance = await prqEth.balanceOf(owner);
      const recipientInitialBalance = await prqBsc.balanceOf(recipient);

      await prqEth.connect(owner).approve(tokenControllerEth.target, amountToTransfer);

      await prqBridgeChainEth.sendToChain(BSC_CHAIN_ID, amountToTransfer, recipient, {
        value: await deBridgeGate.globalFixedNativeFee(),
      });

      await deBridgeGate.callUnlock(prqBridgeChainBsc.target, ETH_CHAIN_ID, amountToTransfer, recipient);

      expect(await prqBsc.balanceOf(recipient)).to.equal(recipientInitialBalance + amountToTransfer);
      expect(await prqBsc.totalSupply()).to.equal(bscInitialTotalSupply + amountToTransfer);
      expect(await prqEth.balanceOf(owner)).to.equal(ownerInitialBalance - amountToTransfer);
    });

    it('should revert with custom error if TokenController not called by bridge', async function () {
      const { owner, tokenControllerEth, otherAccount } = await loadFixture(deployPrqBridgeFixture);

      await expect(
        tokenControllerEth.connect(owner).releaseTokens(otherAccount.address, 10n),
      ).to.be.revertedWithCustomError(tokenControllerEth, 'CallerIsNotABridge');
    });

    it('should revert with custom error if caller is not a debridge gate call proxy', async function () {
      const { prqBridgeChainEth, owner } = await loadFixture(deployPrqBridgeFixture);

      await expect(prqBridgeChainEth.connect(owner).unlock(BSC_CHAIN_ID, 10n, owner)).to.be.revertedWithCustomError(
        prqBridgeChainEth,
        'OnlyProxyCanBeACaller',
      );
    });
  });
});
