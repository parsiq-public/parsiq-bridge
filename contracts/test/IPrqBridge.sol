// SPDX-License-Identifier: MIT

pragma solidity ^0.8.12;

interface IPrqBridge {
  function sendToChain(uint256 _toChainID, uint256 _amount, address _recipient) external;
  function unlock(uint256 fromChainid, uint256 amount, address recipient) external;
}
