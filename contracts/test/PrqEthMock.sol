// SPDX-License-Identifier: MIT

pragma solidity ^0.8.12;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "../ParsiqTokenV2.sol";

contract PrqEthMock is ParsiqToken {}
