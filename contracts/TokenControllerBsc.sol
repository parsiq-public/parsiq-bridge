// SPDX-License-Identifier: MIT
pragma solidity ^0.8.19;

import "./interfaces/ITokenController.sol";
import "./interfaces/IPrqProducer.sol";

import "./controller/TokenController.sol";

contract TokenControllerBsc is TokenController {
  IPrqProducer public immutable prq;

  constructor(IPrqProducer _prq) TokenController() {
    prq = _prq;
  }

  /**
   * @inheritdoc ITokenController
   */
  function releaseTokens(address recipient, uint256 amount) external checkBridge {
    prq.mint(recipient, amount);
  }

  /**
   * @inheritdoc ITokenController
   */
  function reserveTokens(address sender, uint256 amount) external checkBridge {
    prq.burn(sender, amount);
  }
}
