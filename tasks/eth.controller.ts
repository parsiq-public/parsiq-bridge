import { task, types } from 'hardhat/config';
import { TokenControllerEth__factory } from '../typechain-types';
import { sleep } from './sleep';

task('deploy:eth-controller', 'Deploy ETH Controller contract')
  .addParam('prq', 'ETH PRQ token address', undefined, types.string, false)
  .setAction(async ({ prq }, hre) => {
    const [deployer] = await hre.ethers.getSigners();

    console.log('Deploying TokenControllerEth...');

    await hre.deployments.delete('TokenControllerEth');

    const { address, transactionHash } = await hre.deployments.deploy('TokenControllerEth', {
      from: deployer.address,
      args: [prq],
    });

    console.log('TokenControllerEth deploy tx:', transactionHash);
    console.log('TokenControllerEth contract address:', address);

    console.log('Allowing substantial amount of time before validating sources, sleeping for 30 secs');

    await sleep(30000);

    console.log('Verifying TokenControllerEth contract...');

    await hre.run("verify:verify", {
      address: address,
      constructorArguments: [
        prq,
      ],
    });

    return new TokenControllerEth__factory(deployer).attach(address);
  });
