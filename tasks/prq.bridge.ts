import { task, types } from 'hardhat/config';
import { PrqBridge__factory } from '../typechain-types';
import { sleep } from './sleep';

task('deploy:prq-bridge', 'Deploy PrqBridge contract')
  .addParam('deBridgeGate', 'DeBridgeGate address', undefined, types.string, false)
  .addParam('tokenController', 'BSC/ETH Token controller address', undefined, types.string, false)
  .setAction(async ({ deBridgeGate, tokenController }, hre) => {
    const [deployer] = await hre.ethers.getSigners();

    console.log('Deploying PrqBridge...');

    await hre.deployments.delete('PrqBridge');

    const { address, transactionHash } = await hre.deployments.deploy('PrqBridge', {
      from: deployer.address,
      args: [deBridgeGate, tokenController],
    });

    console.log('PrqBridge deploy tx:', transactionHash);
    console.log('PrqBridge contract address:', address);

    console.log('Allowing substantial amount of time before validating sources, sleeping for 30 secs');

    await sleep(30000);

    console.log('Verifying PrqBridge contract...');

    await hre.run('verify:verify', {
      address: address,
      contract: 'contracts/PrqBridge.sol:PrqBridge',
      constructorArguments: [deBridgeGate, tokenController],
    });

    return new PrqBridge__factory(deployer).attach(address);
  });
