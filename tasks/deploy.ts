import { task, types } from 'hardhat/config';
import { TokenControllerBsc } from '../typechain';
import { TokenControllerEth } from '../typechain-types';

task('deploy:bridge-setup', 'Deploy full bridge setup contract')
  .addParam('deBridgeGate', 'DeBridgeGate address', undefined, types.string, false)
  .addParam('prq', 'BSC/ETH PRQ ERC20 address', undefined, types.string, false)
  .addParam('chain', 'BSC/ETH chain selector', undefined, types.string, false)
  .addParam('counterChainBridge', 'Counter Chain Bridge address', undefined, types.string, true)
  .setAction(async ({ deBridgeGate, prq, chain, counterChainBridge }, hre) => {
    const [deployer] = await hre.ethers.getSigners();

    let tokenController;

    if (chain === 'bsc') {
      tokenController = (await hre.run('deploy:bsc-controller', {
        prq: prq,
      })) as TokenControllerBsc;
    }

    if (chain === 'eth') {
      tokenController = (await hre.run('deploy:eth-controller', {
        prq: prq,
      })) as TokenControllerEth;
    }

    if (!tokenController) {
      throw new Error('Invalid chain selector');
    } else {
      const prqBridge = await hre.run('deploy:prq-bridge', {
        deBridgeGate: deBridgeGate,
        tokenController: tokenController.target,
      });

      console.log('BridgeSetup deployed');
      console.log('Chain: ', chain);
      console.log('PRQ Address: ', prq);
      console.log('TokenController Address: ', tokenController.target);
      console.log('PRQBridge Address: ', prqBridge.target);

      await tokenController.connect(deployer).setBridgeContract(prqBridge.target);

      if (counterChainBridge) {
        await prqBridge.connect(deployer).setCounterChainContract(counterChainBridge);
      }

      return {
        chain,
        prq,
        tokenController,
        prqBridge,
      };
    }
  });
