import { task, types } from 'hardhat/config';
import { TokenControllerBsc__factory } from '../typechain-types';
import { sleep } from './sleep';

task('deploy:bsc-controller', 'Deploy BSC Controller contract')
  .addParam('prq', 'BSC PRQ token address', undefined, types.string, false)
  .setAction(async ({ prq }, hre) => {
    const [deployer] = await hre.ethers.getSigners();

    console.log('Deploying TokenControllerBsc...');

    await hre.deployments.delete('TokenControllerBsc');

    const { address, transactionHash } = await hre.deployments.deploy('TokenControllerBsc', {
      from: deployer.address,
      args: [prq],
    });

    console.log('TokenControllerBsc deploy tx:', transactionHash);
    console.log('TokenControllerBsc contract address:', address);

    console.log('Allowing substantial amount of time before validating sources, sleeping for 30 secs');

    await sleep(30000);

    console.log('Verifying TokenControllerBsc contract...');

    await hre.run("verify:verify", {
      address: address,
      constructorArguments: [
        prq,
      ],
    });

    return new TokenControllerBsc__factory(deployer).attach(address);
  });
